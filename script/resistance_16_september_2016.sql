-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Sep 2016 pada 13.24
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resistance`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `games`
--

CREATE TABLE `games` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_GRUP` int(11) DEFAULT NULL,
  `DURATION` int(11) DEFAULT NULL,
  `TOTAL_PLAYER` int(11) DEFAULT NULL,
  `RESULT` int(11) DEFAULT NULL,
  `WINNER` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `games`
--

INSERT INTO `games` (`ID_GAMES`, `ID_GRUP`, `DURATION`, `TOTAL_PLAYER`, `RESULT`, `WINNER`) VALUES
(9, 1, 0, 5, 1, 'resistance'),
(12, 1, 0, 5, 1, 'spy'),
(13, 1, 674, 5, 1, 'resistance'),
(14, 1, 0, 5, 1, 'spy'),
(15, 1, 0, 5, 1, ''),
(17, 1, 0, 5, 1, ''),
(18, 1, 0, 5, 1, 'spy'),
(19, 1, 0, 5, 1, ''),
(20, 1, 549, 5, 1, 'resistance'),
(21, 1, 490, 5, 1, 'spy'),
(25, 1, 0, 5, 1, 'spy'),
(26, 1, 537, 5, 1, 'resistance'),
(27, 1, 735, 5, 1, 'resistance'),
(28, 1, 585, 5, 1, 'spy'),
(29, 1, 0, 7, 1, ''),
(30, 1, 0, 6, 1, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `grup`
--

CREATE TABLE `grup` (
  `ID_GRUP` int(11) NOT NULL,
  `ID_LANG` int(11) DEFAULT NULL,
  `SET_TIME_LEADER` time DEFAULT NULL,
  `SET_TIME_VOTING` time DEFAULT NULL,
  `SET_TIME_MISSION` time DEFAULT NULL,
  `SET_TIME_DISCUSSION` time DEFAULT NULL,
  `ALLOW_FLEE` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `grup`
--

INSERT INTO `grup` (`ID_GRUP`, `ID_LANG`, `SET_TIME_LEADER`, `SET_TIME_VOTING`, `SET_TIME_MISSION`, `SET_TIME_DISCUSSION`, `ALLOW_FLEE`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `language`
--

CREATE TABLE `language` (
  `ID_LANG` int(11) NOT NULL,
  `INDO` text,
  `ENGLISH` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `language`
--

INSERT INTO `language` (`ID_LANG`, `INDO`, `ENGLISH`) VALUES
(1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permainan`
--

CREATE TABLE `permainan` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROLES` varchar(15) NOT NULL,
  `DATE` date NOT NULL,
  `leader` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `permainan`
--

INSERT INTO `permainan` (`ID_GAMES`, `ID_PLAYER`, `ROLES`, `DATE`, `leader`) VALUES
(9, 130434215, 'spy', '2016-09-13', 1),
(9, 230629488, 'spy', '2016-09-13', 1),
(9, 240980442, 'resistance', '2016-09-13', 1),
(9, 243559451, 'resistance', '2016-09-13', 1),
(9, 294641681, 'resistance', '2016-09-13', 1),
(12, 130434215, 'spy', '2016-09-13', 1),
(12, 230629488, 'resistance', '2016-09-13', 1),
(12, 240980442, 'resistance', '2016-09-13', 1),
(12, 243559451, 'resistance', '2016-09-13', 1),
(12, 294641681, 'spy', '2016-09-13', 1),
(13, 130434215, 'resistance', '2016-09-14', 1),
(13, 230629488, 'resistance', '2016-09-14', 1),
(13, 240980442, 'resistance', '2016-09-14', 1),
(13, 243559451, 'resistance', '2016-09-14', 1),
(13, 294641681, 'spy', '2016-09-14', 1),
(14, 130434215, 'spy', '2016-09-14', 1),
(14, 230629488, 'resistance', '2016-09-14', 1),
(14, 240980442, 'resistance', '2016-09-14', 1),
(14, 243559451, 'spy', '2016-09-14', 1),
(14, 294641681, 'resistance', '2016-09-14', 1),
(15, 130434215, 'resistance', '2016-09-14', 1),
(15, 230629488, 'resistance', '2016-09-14', 1),
(15, 240980442, 'spy', '2016-09-14', 1),
(15, 243559451, 'spy', '2016-09-14', 1),
(15, 294641681, 'resistance', '2016-09-14', 1),
(17, 130434215, 'resistance', '2016-09-14', 1),
(17, 230629488, 'spy', '2016-09-14', 1),
(17, 240980442, 'resistance', '2016-09-14', 1),
(17, 243559451, 'resistance', '2016-09-14', 1),
(17, 294641681, 'spy', '2016-09-14', 1),
(18, 130434215, 'resistance', '2016-09-14', 1),
(18, 230629488, 'spy', '2016-09-14', 1),
(18, 240980442, 'resistance', '2016-09-14', 1),
(18, 243559451, 'resistance', '2016-09-14', 1),
(18, 294641681, 'resistance', '2016-09-14', 1),
(19, 130434215, 'spy', '2016-09-14', 1),
(19, 230629488, 'resistance', '2016-09-14', 1),
(19, 240980442, 'resistance', '2016-09-14', 1),
(19, 243559451, 'spy', '2016-09-14', 1),
(19, 294641681, 'resistance', '2016-09-14', 1),
(20, 130434215, 'resistance', '2016-09-14', 1),
(20, 230629488, 'resistance', '2016-09-14', 1),
(20, 240980442, 'resistance', '2016-09-14', 1),
(20, 243559451, 'spy', '2016-09-14', 1),
(20, 294641681, 'spy', '2016-09-14', 1),
(21, 130434215, 'spy', '2016-09-14', 1),
(21, 230629488, 'resistance', '2016-09-14', 1),
(21, 240980442, 'spy', '2016-09-14', 1),
(21, 243559451, 'resistance', '2016-09-14', 1),
(21, 294641681, 'resistance', '2016-09-14', 1),
(25, 130434215, 'resistance', '2016-09-14', 1),
(25, 230629488, 'spy', '2016-09-14', 1),
(25, 240980442, 'spy', '2016-09-14', 1),
(25, 243559451, 'resistance', '2016-09-14', 1),
(25, 294641681, 'resistance', '2016-09-14', 1),
(26, 130434215, 'spy', '2016-09-14', 1),
(26, 230629488, 'resistance', '2016-09-14', 1),
(26, 240980442, 'resistance', '2016-09-14', 1),
(26, 243559451, 'spy', '2016-09-14', 1),
(26, 294641681, 'resistance', '2016-09-14', 1),
(27, 130434215, 'resistance', '2016-09-16', 1),
(27, 230629488, 'resistance', '2016-09-16', 0),
(27, 240980442, 'spy', '2016-09-16', 1),
(27, 243559451, 'resistance', '2016-09-16', 1),
(27, 294641681, 'spy', '2016-09-16', 1),
(28, 130434215, 'spy', '2016-09-16', 1),
(28, 230629488, 'resistance', '2016-09-16', 0),
(28, 240980442, 'spy', '2016-09-16', 1),
(28, 243559451, 'resistance', '2016-09-16', 0),
(28, 294641681, 'resistance', '2016-09-16', 1),
(29, 130434215, 'resistance', '2016-09-16', 0),
(29, 230629488, 'spy', '2016-09-16', 0),
(29, 240980442, 'resistance', '2016-09-16', 1),
(29, 243559451, 'spy', '2016-09-16', 0),
(29, 294641681, 'resistance', '2016-09-16', 1),
(30, 130434215, 'resistance', '2016-09-16', 0),
(30, 230629488, 'resistance', '2016-09-16', 0),
(30, 240980442, 'spy', '2016-09-16', 1),
(30, 243559451, 'spy', '2016-09-16', 0),
(30, 294641681, 'resistance', '2016-09-16', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `player`
--

CREATE TABLE `player` (
  `ID_PLAYER` int(11) NOT NULL,
  `WIN` int(11) DEFAULT NULL,
  `LOSE` int(11) DEFAULT NULL,
  `AS_SPY` int(11) DEFAULT NULL,
  `AS_RESISTANCE` int(11) DEFAULT NULL,
  `TOTAL_MATCH` int(11) DEFAULT NULL,
  `USERNAME` varchar(1024) DEFAULT NULL,
  `NAME` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `player`
--

INSERT INTO `player` (`ID_PLAYER`, `WIN`, `LOSE`, `AS_SPY`, `AS_RESISTANCE`, `TOTAL_MATCH`, `USERNAME`, `NAME`) VALUES
(130434215, 5, 3, 5, 9, 8, 'Dimas_S', 'Dimas Syahputra'),
(172154077, 0, 0, 0, 0, 0, 'yusuf17', 'Hachiman Hikigaya'),
(174407990, 0, 0, 0, 0, 0, 'rezakira', 'Reza F Rahman | Z 4554 VR | 089656148923 '),
(230629488, 6, 2, 4, 10, 8, 'makeLoop', 'Kolo Bolo'),
(240980442, 6, 2, 6, 8, 8, 'guritaDelapan', 'Okto Pus'),
(243559451, 2, 6, 7, 7, 8, 'rapidloco', 'Rapid Lopo'),
(294641681, 1, 7, 4, 10, 8, 'royalKolo', 'Royal Second');

-- --------------------------------------------------------

--
-- Struktur dari tabel `report_mission`
--

CREATE TABLE `report_mission` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `REPORT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `report_mission`
--

INSERT INTO `report_mission` (`ID_GAMES`, `ID_PLAYER`, `ROUND`, `REPORT`) VALUES
(9, 130434215, 2, 1),
(9, 240980442, 2, 1),
(9, 240980442, 3, 1),
(9, 243559451, 1, 1),
(9, 243559451, 2, 1),
(9, 243559451, 3, 1),
(9, 294641681, 1, 1),
(12, 130434215, 2, 1),
(12, 230629488, 1, 1),
(12, 230629488, 3, 1),
(12, 240980442, 2, 1),
(12, 243559451, 1, 0),
(12, 243559451, 2, 0),
(12, 243559451, 3, 0),
(13, 130434215, 1, 1),
(13, 130434215, 2, 1),
(13, 230629488, 1, 1),
(13, 240980442, 2, 1),
(13, 240980442, 3, 1),
(13, 243559451, 2, 1),
(13, 243559451, 3, 1),
(20, 230629488, 2, 1),
(20, 240980442, 2, 1),
(20, 240980442, 3, 1),
(20, 243559451, 1, 1),
(20, 243559451, 2, 1),
(20, 294641681, 1, 1),
(20, 294641681, 3, 1),
(25, 130434215, 2, 1),
(25, 240980442, 1, 1),
(25, 240980442, 2, 1),
(25, 240980442, 3, 1),
(25, 240980442, 4, 0),
(25, 243559451, 1, 1),
(25, 243559451, 2, 0),
(25, 243559451, 4, 0),
(25, 294641681, 3, 0),
(25, 294641681, 4, 1),
(26, 230629488, 2, 1),
(26, 240980442, 2, 1),
(26, 240980442, 3, 1),
(26, 243559451, 1, 1),
(26, 243559451, 3, 1),
(26, 294641681, 1, 1),
(26, 294641681, 2, 1),
(27, 130434215, 1, 1),
(27, 230629488, 2, 1),
(27, 230629488, 3, 1),
(27, 243559451, 1, 1),
(27, 243559451, 2, 1),
(27, 243559451, 3, 1),
(27, 294641681, 2, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `result_mission`
--

CREATE TABLE `result_mission` (
  `ID_GAMES` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `RESULT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `result_mission`
--

INSERT INTO `result_mission` (`ID_GAMES`, `ROUND`, `RESULT`) VALUES
(9, 1, 1),
(9, 2, 1),
(9, 3, 1),
(12, 1, 0),
(12, 2, 0),
(12, 3, 0),
(13, 1, 1),
(13, 2, 1),
(13, 3, 1),
(20, 1, 1),
(20, 2, 1),
(20, 3, 1),
(25, 1, 1),
(25, 2, 0),
(25, 3, 0),
(25, 4, 0),
(26, 1, 1),
(26, 2, 1),
(26, 3, 1),
(27, 1, 1),
(27, 2, 1),
(27, 3, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `result_voting`
--

CREATE TABLE `result_voting` (
  `ID_GAMES` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `RESULT` int(11) NOT NULL,
  `LOOP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `result_voting`
--

INSERT INTO `result_voting` (`ID_GAMES`, `ROUND`, `RESULT`, `LOOP`) VALUES
(9, 1, 1, 1),
(9, 2, 1, 1),
(9, 3, 1, 1),
(12, 1, 1, 1),
(12, 2, 1, 1),
(12, 3, 1, 1),
(13, 1, 1, 1),
(13, 2, 1, 1),
(13, 3, 1, 1),
(14, 1, 0, 1),
(14, 1, 0, 2),
(14, 1, 0, 3),
(15, 1, 0, 1),
(18, 1, 0, 1),
(18, 1, 0, 2),
(18, 1, 0, 3),
(20, 1, 1, 1),
(20, 2, 1, 1),
(20, 3, 1, 1),
(21, 1, 0, 1),
(21, 1, 0, 2),
(21, 1, 0, 3),
(25, 1, 1, 1),
(25, 2, 1, 1),
(25, 3, 1, 1),
(25, 4, 1, 1),
(26, 1, 1, 1),
(26, 2, 1, 1),
(26, 3, 1, 1),
(27, 1, 1, 1),
(27, 2, 1, 1),
(27, 3, 1, 1),
(28, 1, 0, 1),
(28, 1, 0, 2),
(28, 1, 0, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `team`
--

CREATE TABLE `team` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `LOOP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `team`
--

INSERT INTO `team` (`ID_GAMES`, `ID_PLAYER`, `ROUND`, `LOOP`) VALUES
(9, 130434215, 2, 1),
(9, 240980442, 2, 1),
(9, 240980442, 3, 1),
(9, 243559451, 1, 1),
(9, 243559451, 2, 1),
(9, 243559451, 3, 1),
(9, 294641681, 1, 1),
(12, 130434215, 2, 1),
(12, 230629488, 1, 1),
(12, 230629488, 3, 1),
(12, 240980442, 2, 1),
(12, 243559451, 1, 1),
(12, 243559451, 2, 1),
(12, 243559451, 3, 1),
(13, 130434215, 1, 1),
(13, 130434215, 2, 1),
(13, 230629488, 1, 1),
(13, 240980442, 2, 1),
(13, 240980442, 3, 1),
(13, 243559451, 2, 1),
(13, 243559451, 3, 1),
(14, 130434215, 1, 1),
(14, 130434215, 1, 2),
(14, 130434215, 1, 3),
(14, 230629488, 1, 3),
(14, 243559451, 1, 1),
(14, 243559451, 1, 2),
(15, 230629488, 1, 1),
(15, 240980442, 1, 2),
(15, 243559451, 1, 1),
(15, 243559451, 1, 2),
(17, 243559451, 1, 1),
(17, 294641681, 1, 1),
(18, 240980442, 1, 1),
(18, 240980442, 1, 3),
(18, 243559451, 1, 1),
(18, 243559451, 1, 2),
(18, 243559451, 1, 3),
(18, 294641681, 1, 2),
(19, 240980442, 1, 3),
(19, 243559451, 1, 3),
(20, 230629488, 2, 1),
(20, 240980442, 2, 1),
(20, 240980442, 3, 1),
(20, 243559451, 1, 1),
(20, 243559451, 2, 1),
(20, 294641681, 1, 1),
(20, 294641681, 3, 1),
(21, 230629488, 1, 2),
(21, 240980442, 1, 2),
(21, 243559451, 1, 1),
(21, 243559451, 1, 3),
(21, 294641681, 1, 1),
(21, 294641681, 1, 3),
(25, 130434215, 2, 1),
(25, 240980442, 1, 1),
(25, 240980442, 3, 1),
(25, 240980442, 4, 1),
(25, 243559451, 1, 1),
(25, 243559451, 2, 1),
(25, 243559451, 4, 1),
(25, 294641681, 2, 1),
(25, 294641681, 3, 1),
(25, 294641681, 4, 1),
(26, 230629488, 2, 1),
(26, 230629488, 3, 1),
(26, 240980442, 2, 1),
(26, 243559451, 1, 1),
(26, 243559451, 3, 1),
(26, 294641681, 1, 1),
(26, 294641681, 2, 1),
(27, 130434215, 1, 1),
(27, 230629488, 2, 1),
(27, 230629488, 3, 1),
(27, 243559451, 1, 1),
(27, 243559451, 2, 1),
(27, 243559451, 3, 1),
(27, 294641681, 2, 1),
(28, 130434215, 1, 2),
(28, 230629488, 1, 1),
(28, 230629488, 1, 3),
(28, 243559451, 1, 1),
(28, 243559451, 1, 2),
(28, 243559451, 1, 3),
(30, 130434215, 1, 1),
(30, 243559451, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `text_mission`
--

CREATE TABLE `text_mission` (
  `ID_MISSION` int(11) NOT NULL,
  `TEXT` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `text_mission`
--

INSERT INTO `text_mission` (`ID_MISSION`, `TEXT`) VALUES
(0, 'Mencuri sendal di istana merdeka'),
(1, 'Buat kegaduhan di Monas'),
(2, 'Mencuri emas milik Monas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `voting`
--

CREATE TABLE `voting` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `VOTE` int(11) NOT NULL,
  `LOOP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `voting`
--

INSERT INTO `voting` (`ID_GAMES`, `ID_PLAYER`, `ROUND`, `VOTE`, `LOOP`) VALUES
(9, 130434215, 1, 1, 0),
(9, 130434215, 2, 1, 0),
(9, 130434215, 3, 1, 0),
(9, 230629488, 1, 1, 0),
(9, 230629488, 2, 1, 0),
(9, 230629488, 3, 1, 0),
(9, 240980442, 1, 1, 0),
(9, 240980442, 2, 1, 0),
(9, 240980442, 3, 1, 0),
(9, 243559451, 1, 1, 0),
(9, 243559451, 2, 1, 0),
(9, 243559451, 3, 1, 0),
(9, 294641681, 1, 1, 0),
(9, 294641681, 2, 1, 0),
(9, 294641681, 3, 1, 0),
(12, 130434215, 1, 1, 0),
(12, 130434215, 2, 1, 0),
(12, 130434215, 3, 1, 0),
(12, 230629488, 1, 1, 0),
(12, 230629488, 2, 1, 0),
(12, 230629488, 3, 1, 0),
(12, 240980442, 1, 1, 0),
(12, 240980442, 2, 1, 0),
(12, 240980442, 3, 0, 0),
(12, 243559451, 1, 1, 0),
(12, 243559451, 2, 1, 0),
(12, 243559451, 3, 1, 0),
(12, 294641681, 1, 1, 0),
(12, 294641681, 2, 1, 0),
(12, 294641681, 3, 0, 0),
(13, 130434215, 1, 1, 0),
(13, 130434215, 2, 1, 0),
(13, 130434215, 3, 1, 0),
(13, 230629488, 1, 1, 0),
(13, 230629488, 2, 1, 0),
(13, 230629488, 3, 1, 0),
(13, 240980442, 1, 1, 0),
(13, 240980442, 2, 1, 0),
(13, 240980442, 3, 1, 0),
(13, 243559451, 1, 1, 0),
(13, 243559451, 2, 1, 0),
(13, 243559451, 3, 1, 0),
(13, 294641681, 1, 1, 0),
(13, 294641681, 2, 1, 0),
(13, 294641681, 3, 1, 0),
(14, 130434215, 1, 1, 0),
(14, 230629488, 1, 0, 0),
(14, 240980442, 1, 0, 0),
(14, 243559451, 1, 0, 0),
(14, 294641681, 1, 1, 0),
(15, 130434215, 1, 0, 1),
(15, 130434215, 1, 0, 2),
(15, 230629488, 1, 0, 1),
(15, 230629488, 1, 0, 2),
(15, 240980442, 1, 0, 1),
(15, 240980442, 1, 0, 2),
(15, 243559451, 1, 0, 1),
(15, 243559451, 1, 0, 2),
(15, 294641681, 1, 0, 1),
(15, 294641681, 1, 0, 2),
(17, 243559451, 1, 0, 1),
(18, 130434215, 1, 0, 1),
(18, 130434215, 1, 0, 2),
(18, 130434215, 1, 0, 3),
(18, 230629488, 1, 0, 1),
(18, 230629488, 1, 0, 2),
(18, 230629488, 1, 0, 3),
(18, 240980442, 1, 0, 1),
(18, 240980442, 1, 0, 2),
(18, 240980442, 1, 0, 3),
(18, 243559451, 1, 0, 1),
(18, 243559451, 1, 0, 2),
(18, 243559451, 1, 0, 3),
(18, 294641681, 1, 0, 1),
(18, 294641681, 1, 0, 2),
(18, 294641681, 1, 0, 3),
(19, 130434215, 1, 1, 3),
(19, 230629488, 1, 1, 3),
(19, 240980442, 1, 1, 3),
(19, 243559451, 1, 1, 3),
(19, 294641681, 1, 1, 3),
(20, 130434215, 1, 1, 1),
(20, 130434215, 2, 1, 1),
(20, 130434215, 3, 1, 1),
(20, 230629488, 1, 1, 1),
(20, 230629488, 2, 1, 1),
(20, 230629488, 3, 1, 1),
(20, 240980442, 1, 1, 1),
(20, 240980442, 2, 1, 1),
(20, 240980442, 3, 1, 1),
(20, 243559451, 1, 1, 1),
(20, 243559451, 2, 1, 1),
(20, 243559451, 3, 1, 1),
(20, 294641681, 1, 1, 1),
(20, 294641681, 2, 1, 1),
(20, 294641681, 3, 1, 1),
(21, 130434215, 1, 0, 1),
(21, 130434215, 1, 0, 2),
(21, 130434215, 1, 0, 3),
(21, 230629488, 1, 0, 1),
(21, 230629488, 1, 0, 2),
(21, 230629488, 1, 0, 3),
(21, 240980442, 1, 0, 1),
(21, 240980442, 1, 0, 2),
(21, 240980442, 1, 0, 3),
(21, 243559451, 1, 0, 1),
(21, 243559451, 1, 0, 2),
(21, 243559451, 1, 0, 3),
(21, 294641681, 1, 1, 1),
(21, 294641681, 1, 0, 2),
(21, 294641681, 1, 0, 3),
(25, 130434215, 1, 1, 1),
(25, 130434215, 2, 1, 1),
(25, 130434215, 3, 1, 1),
(25, 130434215, 4, 1, 1),
(25, 230629488, 1, 1, 1),
(25, 230629488, 2, 0, 1),
(25, 230629488, 3, 0, 1),
(25, 230629488, 4, 1, 1),
(25, 240980442, 1, 1, 1),
(25, 240980442, 2, 1, 1),
(25, 240980442, 3, 1, 1),
(25, 240980442, 4, 1, 1),
(25, 243559451, 1, 0, 1),
(25, 243559451, 2, 1, 1),
(25, 243559451, 3, 1, 1),
(25, 243559451, 4, 1, 1),
(25, 294641681, 1, 1, 1),
(25, 294641681, 2, 1, 1),
(25, 294641681, 3, 1, 1),
(25, 294641681, 4, 1, 1),
(26, 130434215, 1, 1, 1),
(26, 130434215, 2, 1, 1),
(26, 130434215, 3, 1, 1),
(26, 230629488, 1, 1, 1),
(26, 230629488, 2, 1, 1),
(26, 230629488, 3, 1, 1),
(26, 240980442, 1, 1, 1),
(26, 240980442, 2, 1, 1),
(26, 240980442, 3, 1, 1),
(26, 243559451, 1, 1, 1),
(26, 243559451, 2, 1, 1),
(26, 243559451, 3, 1, 1),
(26, 294641681, 1, 1, 1),
(26, 294641681, 2, 1, 1),
(26, 294641681, 3, 1, 1),
(27, 130434215, 1, 1, 1),
(27, 130434215, 2, 1, 1),
(27, 130434215, 3, 1, 1),
(27, 230629488, 1, 1, 1),
(27, 230629488, 2, 1, 1),
(27, 230629488, 3, 1, 1),
(27, 240980442, 1, 1, 1),
(27, 240980442, 2, 1, 1),
(27, 240980442, 3, 1, 1),
(27, 243559451, 1, 1, 1),
(27, 243559451, 2, 1, 1),
(27, 243559451, 3, 1, 1),
(27, 294641681, 1, 1, 1),
(27, 294641681, 2, 1, 1),
(27, 294641681, 3, 1, 1),
(28, 130434215, 1, 0, 1),
(28, 130434215, 1, 0, 2),
(28, 130434215, 1, 0, 3),
(28, 230629488, 1, 0, 1),
(28, 230629488, 1, 0, 2),
(28, 230629488, 1, 0, 3),
(28, 240980442, 1, 0, 1),
(28, 240980442, 1, 0, 2),
(28, 240980442, 1, 0, 3),
(28, 243559451, 1, 0, 1),
(28, 243559451, 1, 0, 2),
(28, 243559451, 1, 0, 3),
(28, 294641681, 1, 0, 1),
(28, 294641681, 1, 0, 2),
(28, 294641681, 1, 0, 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_permainan`
--
CREATE TABLE `v_permainan` (
`ID_GAMES` int(11)
,`ID_PLAYER` int(11)
,`ROLES` varchar(15)
,`DATE` date
,`leader` int(11)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_permainan`
--
DROP TABLE IF EXISTS `v_permainan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_permainan`  AS  (select `permainan`.`ID_GAMES` AS `ID_GAMES`,`permainan`.`ID_PLAYER` AS `ID_PLAYER`,`permainan`.`ROLES` AS `ROLES`,`permainan`.`DATE` AS `DATE`,`permainan`.`leader` AS `leader` from `permainan` where (`permainan`.`leader` = 0)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`ID_GAMES`),
  ADD KEY `FK_RELATIONSHIP_4` (`ID_GRUP`);

--
-- Indexes for table `grup`
--
ALTER TABLE `grup`
  ADD PRIMARY KEY (`ID_GRUP`),
  ADD KEY `FK_RELATIONSHIP_3` (`ID_LANG`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`ID_LANG`);

--
-- Indexes for table `permainan`
--
ALTER TABLE `permainan`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`ID_PLAYER`);

--
-- Indexes for table `report_mission`
--
ALTER TABLE `report_mission`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`,`ROUND`,`REPORT`);

--
-- Indexes for table `result_mission`
--
ALTER TABLE `result_mission`
  ADD PRIMARY KEY (`ID_GAMES`,`ROUND`);

--
-- Indexes for table `result_voting`
--
ALTER TABLE `result_voting`
  ADD PRIMARY KEY (`ID_GAMES`,`ROUND`,`RESULT`,`LOOP`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`,`ROUND`,`LOOP`);

--
-- Indexes for table `text_mission`
--
ALTER TABLE `text_mission`
  ADD PRIMARY KEY (`ID_MISSION`);

--
-- Indexes for table `voting`
--
ALTER TABLE `voting`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`,`ROUND`,`LOOP`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `ID_GAMES` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`ID_GRUP`) REFERENCES `grup` (`ID_GRUP`);

--
-- Ketidakleluasaan untuk tabel `grup`
--
ALTER TABLE `grup`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`ID_LANG`) REFERENCES `language` (`ID_LANG`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
