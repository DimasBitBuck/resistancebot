-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Sep 2016 pada 11.29
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resistance`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `games`
--

CREATE TABLE `games` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_GRUP` int(11) DEFAULT NULL,
  `DURATION` int(11) DEFAULT NULL,
  `TOTAL_PLAYER` int(11) DEFAULT NULL,
  `RESULT` int(11) DEFAULT NULL,
  `WINNER` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `games`
--

INSERT INTO `games` (`ID_GAMES`, `ID_GRUP`, `DURATION`, `TOTAL_PLAYER`, `RESULT`, `WINNER`) VALUES
(0, 1, 0, 5, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `grup`
--

CREATE TABLE `grup` (
  `ID_GRUP` int(11) NOT NULL,
  `ID_LANG` int(11) DEFAULT NULL,
  `SET_TIME_LEADER` time DEFAULT NULL,
  `SET_TIME_VOTING` time DEFAULT NULL,
  `SET_TIME_MISSION` time DEFAULT NULL,
  `SET_TIME_DISCUSSION` time DEFAULT NULL,
  `ALLOW_FLEE` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `grup`
--

INSERT INTO `grup` (`ID_GRUP`, `ID_LANG`, `SET_TIME_LEADER`, `SET_TIME_VOTING`, `SET_TIME_MISSION`, `SET_TIME_DISCUSSION`, `ALLOW_FLEE`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `language`
--

CREATE TABLE `language` (
  `ID_LANG` int(11) NOT NULL,
  `INDO` text,
  `ENGLISH` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `language`
--

INSERT INTO `language` (`ID_LANG`, `INDO`, `ENGLISH`) VALUES
(1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permainan`
--

CREATE TABLE `permainan` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROLES` varchar(15) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `leader` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `permainan`
--

INSERT INTO `permainan` (`ID_GAMES`, `ID_PLAYER`, `ROLES`, `DATE`, `leader`) VALUES
(0, 130434215, 'spy', '2016-09-09', 1),
(0, 230629488, 'spy', '2016-09-09', 0),
(0, 240980442, 'resistance', '2016-09-09', 0),
(0, 243559451, 'resistance', '2016-09-09', 0),
(0, 294641681, 'resistance', '2016-09-09', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `player`
--

CREATE TABLE `player` (
  `ID_PLAYER` int(11) NOT NULL,
  `WIN` int(11) DEFAULT NULL,
  `LOSE` int(11) DEFAULT NULL,
  `AS_SPY` int(11) DEFAULT NULL,
  `AS_RESISTANCE` int(11) DEFAULT NULL,
  `TOTAL_MATCH` int(11) DEFAULT NULL,
  `USERNAME` varchar(1024) DEFAULT NULL,
  `NAME` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `player`
--

INSERT INTO `player` (`ID_PLAYER`, `WIN`, `LOSE`, `AS_SPY`, `AS_RESISTANCE`, `TOTAL_MATCH`, `USERNAME`, `NAME`) VALUES
(130434215, 0, 0, 0, 0, 0, 'Dimas_S', 'Dimas Syahputra'),
(172154077, 0, 0, 0, 0, 0, 'yusuf17', 'Hachiman Hikigaya'),
(174407990, 0, 0, 0, 0, 0, 'rezakira', 'Reza F Rahman | Z 4554 VR | 089656148923 '),
(230629488, 0, 0, 0, 0, 0, 'makeLoop', 'Kolo Bolo'),
(240980442, 0, 0, 0, 0, 0, 'guritaDelapan', 'Okto Pus'),
(243559451, 0, 0, 0, 0, 0, 'rapidloco', 'Rapid Lopo'),
(294641681, 0, 0, 0, 0, 0, 'royalKolo', 'Royal Second');

-- --------------------------------------------------------

--
-- Struktur dari tabel `report_mission`
--

CREATE TABLE `report_mission` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `REPORT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `report_mission`
--

INSERT INTO `report_mission` (`ID_GAMES`, `ID_PLAYER`, `ROUND`, `REPORT`) VALUES
(0, 230629488, 1, 1),
(0, 243559451, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `result_mission`
--

CREATE TABLE `result_mission` (
  `ID_GAMES` int(11) NOT NULL,
  `ROUND` int(11) DEFAULT NULL,
  `RESULT` int(11) DEFAULT NULL,
  `ID_RESULT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `result_voting`
--

CREATE TABLE `result_voting` (
  `ID_GAMES` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `RESULT` int(11) NOT NULL,
  `LOOP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `result_voting`
--

INSERT INTO `result_voting` (`ID_GAMES`, `ROUND`, `RESULT`, `LOOP`) VALUES
(0, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `team`
--

CREATE TABLE `team` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `LOOP` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `team`
--

INSERT INTO `team` (`ID_GAMES`, `ID_PLAYER`, `ROUND`, `LOOP`) VALUES
(0, 230629488, 1, 1),
(0, 243559451, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `text_mission`
--

CREATE TABLE `text_mission` (
  `ID_MISSION` int(11) NOT NULL,
  `TEXT` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `text_mission`
--

INSERT INTO `text_mission` (`ID_MISSION`, `TEXT`) VALUES
(0, 'Mencuri sendal di istana merdeka'),
(1, 'Buat kegaduhan di Monas'),
(2, 'Mencuri emas milik Monas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `voting`
--

CREATE TABLE `voting` (
  `ID_GAMES` int(11) NOT NULL,
  `ID_PLAYER` int(11) NOT NULL,
  `ROUND` int(11) NOT NULL,
  `VOTE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `voting`
--

INSERT INTO `voting` (`ID_GAMES`, `ID_PLAYER`, `ROUND`, `VOTE`) VALUES
(0, 130434215, 1, 1),
(0, 230629488, 1, 1),
(0, 240980442, 1, 1),
(0, 243559451, 1, 1),
(0, 294641681, 1, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_permainan`
--
CREATE TABLE `v_permainan` (
`ID_GAMES` int(11)
,`ID_PLAYER` int(11)
,`ROLES` varchar(15)
,`DATE` date
,`leader` int(11)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_permainan`
--
DROP TABLE IF EXISTS `v_permainan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_permainan`  AS  (select `permainan`.`ID_GAMES` AS `ID_GAMES`,`permainan`.`ID_PLAYER` AS `ID_PLAYER`,`permainan`.`ROLES` AS `ROLES`,`permainan`.`DATE` AS `DATE`,`permainan`.`leader` AS `leader` from `permainan` where (`permainan`.`leader` = 0)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`ID_GAMES`),
  ADD KEY `FK_RELATIONSHIP_4` (`ID_GRUP`);

--
-- Indexes for table `grup`
--
ALTER TABLE `grup`
  ADD PRIMARY KEY (`ID_GRUP`),
  ADD KEY `FK_RELATIONSHIP_3` (`ID_LANG`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`ID_LANG`);

--
-- Indexes for table `permainan`
--
ALTER TABLE `permainan`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`),
  ADD KEY `FK_RELATIONSHIP_2` (`ID_PLAYER`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`ID_PLAYER`);

--
-- Indexes for table `report_mission`
--
ALTER TABLE `report_mission`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`,`ROUND`,`REPORT`);

--
-- Indexes for table `result_mission`
--
ALTER TABLE `result_mission`
  ADD PRIMARY KEY (`ID_RESULT`),
  ADD KEY `FK_REFERENCE_10` (`ID_GAMES`);

--
-- Indexes for table `result_voting`
--
ALTER TABLE `result_voting`
  ADD PRIMARY KEY (`ID_GAMES`,`ROUND`,`RESULT`,`LOOP`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`,`ROUND`,`LOOP`);

--
-- Indexes for table `text_mission`
--
ALTER TABLE `text_mission`
  ADD PRIMARY KEY (`ID_MISSION`);

--
-- Indexes for table `voting`
--
ALTER TABLE `voting`
  ADD PRIMARY KEY (`ID_GAMES`,`ID_PLAYER`,`ROUND`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`ID_GRUP`) REFERENCES `grup` (`ID_GRUP`);

--
-- Ketidakleluasaan untuk tabel `grup`
--
ALTER TABLE `grup`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`ID_LANG`) REFERENCES `language` (`ID_LANG`);

--
-- Ketidakleluasaan untuk tabel `permainan`
--
ALTER TABLE `permainan`
  ADD CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`ID_GAMES`) REFERENCES `games` (`ID_GAMES`),
  ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`ID_PLAYER`) REFERENCES `player` (`ID_PLAYER`);

--
-- Ketidakleluasaan untuk tabel `result_mission`
--
ALTER TABLE `result_mission`
  ADD CONSTRAINT `FK_REFERENCE_10` FOREIGN KEY (`ID_GAMES`) REFERENCES `games` (`ID_GAMES`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
